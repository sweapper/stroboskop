# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://sweapper@bitbucket.org/sweapper/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/sweapper/stroboskop/commits/70db8a1154e5a398d2b95a403355c61cece43670

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/sweapper/stroboskop/commits/1570e6ea006739a6db6dc4f8476f39f8afc5cdd5

Naloga 6.3.2:
https://bitbucket.org/sweapper/stroboskop/commits/565574de2ffb7fe59e8620c9efe3d7719992e6c3

Naloga 6.3.3:
https://bitbucket.org/sweapper/stroboskop/commits/9458301b988a450a9ea8aa4a41377d40048f9575

Naloga 6.3.4:
https://bitbucket.org/sweapper/stroboskop/commits/beef460dc3660117a835d4ab2e754442805396f0

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/sweapper/stroboskop/commits/44bc77192026f8df98e14b6cbe4446d79b572af0

Naloga 6.4.2:
https://bitbucket.org/sweapper/stroboskop/commits/a22cc9ec7c40b7c539351df823a68120b69d8d0e

Naloga 6.4.3:
https://bitbucket.org/sweapper/stroboskop/commits/2b01b1766280168ac7594ce2041b190b9649672b

Naloga 6.4.4:
https://bitbucket.org/sweapper/stroboskop/commits/b7459fcf77aefca80dd4785b48bccb05b9407104